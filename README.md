# Diagram Maker for Engineers
[![pipeline status](https://gitlab.com/alsaffar/diagram-maker/badges/master/pipeline.svg)](https://gitlab.com/alsaffar/diagram-maker/commits/master)

# Demo
A demo website which demonstrates how this library works can be access from this address: [https://alsaffar.gitlab.io/diagram-maker/](https://alsaffar.gitlab.io/diagram-maker/)