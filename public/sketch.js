function setup() {
    createCanvas(.8* 1000, .8 *1000);
    background(255)
    createDiv('Adjust sliders to change Dimensions:');
    topWidth = createSlider(0.1, 10, 2.01, 0);
    //topWidth.position(10, 10);
    topWidth.style('width', '150px');
    topThickness = createSlider(0.1, 10, 0.324, 0);
    //topThickness.position(10, 30);
    topThickness.style('width', '150px');
    
    webT = createSlider(0.1, 10, 0.260, 0);
    //webT.position(10, 50);
    webT.style('width', '150px');
    webH = createSlider(0.1, 10, 3.754 - 0.303 - 0.324, 0);
    //webH.position(10, 70);
    webH.style('width', '150px');
    
    botWidth = createSlider(0.1, 10, 3.01, 0);
   // botWidth.position(10, 90);
    botWidth.style('width', '150px');
    botThickness = createSlider(0.1, 10, 0.303, 0);
   // botThickness.position(10, 110);
    botThickness.style('width', '150px');

    rotateAmount = createSlider(- PI, PI, 0, 0);
   // rotateAmount.position(10, 130);
    rotateAmount.style('width', '150px');

    textFont("Monospace")
    createDiv('Save your diagram by clicking this button:');
    captureDiagram = createButton("Save Diagram")
    captureDiagram.mousePressed(captureTheDiagram)
}

function captureTheDiagram() {
    saveCanvas();
}

function draw() {
    background(255)
    //text("x: " + mouseX + ", y: " + mouseY, 10, 170)
    drawISection(topWidth.value(), topThickness.value(), webT.value(), webH.value(), botWidth.value(), botThickness.value(), true, "in.", rotateAmount.value());
}

function drawISection(topFlangeWidth, topFlangeThickness, webThickness, webHeight, bottomFlangeWidth, bottomFlangeThickness, writeDimensions, unitString, orientation) {
        heightNormalizor = topFlangeThickness + webHeight + bottomFlangeThickness;
        widthNormalizor = max(topFlangeWidth, webThickness, bottomFlangeWidth);
        heightNormalizor = max(heightNormalizor, widthNormalizor);
        widthNormalizor = heightNormalizor;
        topFlangeWidth = (.8 * width - 20) * topFlangeWidth / widthNormalizor;
        topFlangeThickness = (.8 * height - 20) * topFlangeThickness / heightNormalizor;
        webThickness = (.8 * width - 20) * webThickness / widthNormalizor;
        webHeight = (.8 * height - 20) * webHeight / heightNormalizor;
        bottomFlangeWidth = (.8 * width - 20) * bottomFlangeWidth / widthNormalizor;
        bottomFlangeThickness = (.8 * height - 20) * bottomFlangeThickness / heightNormalizor;
        totalHeight = topFlangeThickness + webHeight + bottomFlangeThickness;
        push();
            stroke(0, 0, 0)
            translate(width / 2, height / 2)
            rotate(orientation)
            fill(100, 20)
            beginShape();
                vertex( - (topFlangeWidth / 2),-(totalHeight / 2));
                vertex((topFlangeWidth / 2),- (totalHeight / 2))
                vertex((topFlangeWidth / 2), -((totalHeight / 2) - topFlangeThickness))
                vertex((webThickness / 2),- ((totalHeight / 2) - topFlangeThickness))       
                vertex((webThickness / 2),- ((- totalHeight / 2) + bottomFlangeThickness)) 
                vertex((bottomFlangeWidth / 2), -((- totalHeight / 2) + bottomFlangeThickness))
                vertex((bottomFlangeWidth / 2),- (- totalHeight / 2))
                vertex((- bottomFlangeWidth / 2),- (- totalHeight / 2))
                vertex(( - bottomFlangeWidth / 2), -((- totalHeight / 2) + bottomFlangeThickness))
                vertex((- webThickness / 2), -((- totalHeight / 2) + bottomFlangeThickness))
                vertex((- webThickness / 2), -((totalHeight / 2) - topFlangeThickness))
                vertex((- topFlangeWidth / 2), -((totalHeight / 2) - topFlangeThickness))
                vertex( - (topFlangeWidth / 2),-(totalHeight / 2));
            endShape();
            fill(0, 0, 0)
            if(writeDimensions) {
                stroke(0, 0, 255, 50)
                //Top width
                line(- (topFlangeWidth / 2), -(totalHeight / 2) - 5, - (topFlangeWidth / 2), -(totalHeight / 2) - 35);
                line( (topFlangeWidth / 2), -(totalHeight / 2) - 5,  (topFlangeWidth / 2), -(totalHeight / 2) - 35);
                line((- topFlangeWidth / 2), -(totalHeight / 2) - 5 - 15, (topFlangeWidth / 2), -(totalHeight / 2) - 5- 15);
                push()
                translate(0, - totalHeight/2 - 25)
                rotate(-orientation)
                textSize(14)
                text("" + topWidth.value() + " " + unitString, 0, 0)
                pop()
                //bottom width
                line(- (bottomFlangeWidth / 2), (totalHeight / 2) + 5, - (bottomFlangeWidth / 2), (totalHeight / 2) + 35);
                line( (bottomFlangeWidth / 2), (totalHeight / 2) + 5,  (bottomFlangeWidth / 2), (totalHeight / 2) + 35);
                line((- bottomFlangeWidth / 2), (totalHeight / 2) + 5 + 15, (bottomFlangeWidth / 2), (totalHeight / 2) + 5+ 15);
                push()
                translate(0, totalHeight/2 + 45)
                rotate(-orientation)
                textSize(14)
                text("" + (bottomFlangeWidth * widthNormalizor / (.8 * width - 20)) + " " + unitString, 0, 0)
                pop()
                //web thickness
                line(-webThickness / 2 - 10, -20, - webThickness/2 + 10, 20)
                line(webThickness / 2 - 10, -20, webThickness/2 + 10, 20)
                line(-webThickness/2 -5, 0, webThickness/2 + 5, 0)
                push()
                translate(webThickness/2 + 15, 0)
                rotate(-orientation)
                textSize(14)
                text("" + (webThickness * widthNormalizor / (.8 * width - 20)) + " " + unitString, 0, 0)
                pop()
                //all other thicknesses + web height
                push()
                translate(-webThickness, 0)
                line(0, totalHeight/2 + 10, 0, -totalHeight/2 - 10)
                line(-20, totalHeight/2 - 10, 20, totalHeight/2 + 10)
                line(-20, totalHeight/2 - 10 - bottomFlangeThickness, 20, totalHeight/2 + 10 - bottomFlangeThickness)
                line(-20, totalHeight/2 - 10 - bottomFlangeThickness - webHeight, 20, totalHeight/2 + 10 - bottomFlangeThickness - webHeight)
                line(-20, totalHeight/2 - 10 - bottomFlangeThickness - webHeight - topFlangeThickness, 20, totalHeight/2 + 10 - bottomFlangeThickness - webHeight - topFlangeThickness)
                pop()
                push()
                translate(-webThickness - 5, 0)
                rotate(-PI / 2)
                textSize(14)
                text("" + (webHeight * heightNormalizor / (.8 * height - 20)) + " " + unitString, 0, 0)
                pop()
                push()
                translate(-webThickness + 5, -totalHeight/2 + topFlangeThickness/2)
                textSize(14)
                text("" + (topFlangeThickness * heightNormalizor / (.8 * height - 20)) + " " + unitString, 0, 0)
                pop()
                push()
                translate(-webThickness + 5, totalHeight/2  - bottomFlangeThickness/2)
                textSize(14)
                text("" + (bottomFlangeThickness * heightNormalizor / (.8 * height - 20)) + " " + unitString, 0, 0)
                pop()
            }
        pop();
}

